# **<div align="center">Graph Search</div>**

## **Introduction**

A graph is a non-linear data structure that is used to implement the mathematical concept of graphs. It is basically a collection of vertices and edges that connect these vertices. These vertices also called nodes. A graph can be viewed as a generalization of the tree structure.

Mathematically, graphs are represented as “G = {V, E}”, where V and E are lists of all the vertices and edges in the graph[1].

![Graph](graph.png)

## **Graph Search Algorithm**

There are two basic types of graph search algorithms: depth-first and breadth-first. These algorithms specify an order to search through
the nodes of a graph. We start at the source node and keep searching until we find the target node.

### **Breadth First Search (BFS)**

Breadth-first search (BFS) is a graph search algorithm that begins at the root node and explores all the neighboring nodes. Then for each of those nearest nodes, the algorithm explores their unexplored neighbor nodes, and so, until it finds the goal.

This is accomplished by using a queue. Below algorithm [2] is shown -

```
Set all nodes to "not visited";

   q = new Queue();
   q.enqueue(initial node);

   while ( q ≠ empty ) do
   {
      x = q.dequeue();

      if ( x has not been visited )
      {
         visited[x] = true;

         for ( every edge (x, y) )
            if ( y has not been visited )
                q.enqueue(y);
      }
   }
```

In the above algorithm, We took an empty queue and selected a starting node (usually root node) and insert it into the queue. If the queue is not empty,extract the node from the queue and insert its child nodes (if not visited) into the queue.

Breadth-first search can be used to solve many problems -

- We can find individuals within a given distance 'k' from a person until 'k' levels on social networks.
- Crawlers create an index using Breadth-First. The idea is to start from the source page and follow all links from the source and keep doing the same.

### **Depth First Search (DFS)**

Depth-first search algorithm[3] shown below progresses by expanding the starting node of G and then going deeper and deeper until the goal node is found, or until a node that has no children is encountered. When a dead-end is reached, the algorithm backtracks, returning to the most recent node that has not been completely explored.

```
Set all nodes to "not visited";

   s = new Stack();

   s.push(initial node);

   while ( s ≠ empty ) do
   {
      x = s.pop();
      if ( x has not been visited )
      {
         visited[x] = true;

         for ( every edge (x, y) )
            if ( y has not been visited )
                s.push(y);
      }
   }
```

It's implementation is similar to breadth-first search but here we use a stack instead of queue.

Depth-first search can be used to solve many problems -

- Detecting cycle in a graph
- For a weighted graph, DFS traversal of the graph produces the minimum spanning tree and all pair shortest path tree.

### **Conclusion**

Graph search algorithms help you traverse a graph dataset in the most efficient means possible. But “most efficient” depends on the results you’re looking for – a breadth-first search isn’t the most efficient if your results are better suited to depth-first queries (and vice versa).

[1] https://medium.com/@dakota.lillie/coming-to-terms-with-graph-search-c489a44e5581

[1] http://www.mathcs.emory.edu/~cheung/Courses/171/Syllabus/11-Graph/bfs.html

[2] http://www.mathcs.emory.edu/~cheung/Courses/171/Syllabus/11-Graph/dfs.html
